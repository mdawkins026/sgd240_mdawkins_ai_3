// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "AI_3GameMode.h"
#include "AI_3Character.h"
#include "UObject/ConstructorHelpers.h"

AAI_3GameMode::AAI_3GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
