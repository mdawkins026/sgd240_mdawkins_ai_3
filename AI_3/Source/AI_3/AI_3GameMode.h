// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AI_3GameMode.generated.h"

UCLASS(minimalapi)
class AAI_3GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAI_3GameMode();
};



