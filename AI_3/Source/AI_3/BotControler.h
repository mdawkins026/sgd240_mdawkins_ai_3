// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BotControler.generated.h"

/**
 * 
 */
UCLASS()
class AI_3_API ABotControler : public AAIController
{
	GENERATED_BODY()

public:
	ABotControler();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnPossess(APawn * InPawn) override;

	class ABot* ControlledPawn;

	void Seek(AActor* target);

	void Seek(FVector target);

	void Flee(AActor* target);

	void Flee(FVector target);

	void Arrive(AActor* target, float slowingRadius);

	void Wander();

	void Pursuit(AActor* target);

	void Evade(AActor* target);
};
