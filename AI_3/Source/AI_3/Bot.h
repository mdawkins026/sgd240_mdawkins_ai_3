// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Bot.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ESteeringBehaviourType : uint8
{
	BC_Seek 	UMETA(DisplayName = "Seek"),
	BC_Flee 	UMETA(DisplayName = "Flee"),
	BC_Arrive	UMETA(DisplayName = "Arrive"),
	BC_Wander	UMETA(DisplayName = "Wander"),
	BC_Pursuit	UMETA(DisplayName = "Pursuit"),
	BC_Evade	UMETA(DisplayName = "Evade"),
};

UCLASS()
class AI_3_API ABot : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(BlueprintReadWrite, category = "rootComp")
	class UCapsuleComponent* rootComp;

	UPROPERTY(VisibleAnywhere)
	class  UFloatingPawnMovement* movementComp;

	UPROPERTY(EditAnywhere, category = "SteeringBehaviourParams")
	ESteeringBehaviourType behvaiour;

	UPROPERTY(EditAnywhere, category = "SteeringBehaviourParams")
	AActor* Target;

	UPROPERTY(EditAnywhere, category = "SteeringBehaviourParams")
	float maxForce = 5.0f;

	UPROPERTY(EditAnywhere, category = "SteeringBehaviourParams")
	TArray<AActor*> walls;

	UPROPERTY(EditAnywhere, category = "SteeringBehaviourParams")
	class UStaticMeshComponent* mesh;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	int NunberOfBotRows = 5;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	int NunberOfBotCols = 5;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	float NeighborhoodRadius = 500.0f;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	float CohesionScaler = 0.0f;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	float SeperationScaler = 0.0f;

	UFUNCTION()
	void OnHitWall(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditAnywhere, category = "SteeringBehaviourParams")
	float AcceptanceDistance = 50.0f;
};
