// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bot.h"
#include "BotFlock.generated.h"

UCLASS()
class AI_3_API ABotFlock : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABotFlock();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnConstruction(const FTransform & Transform) override;

	UPROPERTY(EditAnywhere)
	class USceneComponent* RootComp;

	UPROPERTY(EditAnywhere, category = "SteeringBehaviourParams")
	ESteeringBehaviourType behvaiour = ESteeringBehaviourType::BC_Wander;

	UPROPERTY(EditAnywhere, category = "SteeringBehaviourParams")
	float maxForce = 5.0f;

	UPROPERTY(EditAnywhere, category = "SteeringBehaviourParams")
	TArray<AActor*> walls;

	UPROPERTY(EditAnywhere, category = "SteeringBehaviourParams")
	AActor* Target;

	UPROPERTY(VisibleAnywhere)
	TArray<class ABot*> BotsInTheFlock;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	int NunberOfBotRows = 5;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	int NunberOfBotCols = 5;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	float IntialSpacingInterval = 200.0f;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	float NeighborhoodRadius = 500.0f;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	float CohesionScaler = 1.0f;

	UPROPERTY(EditAnywhere, Category = "FlockBehaviour")
	float SeperationScaler = 1.0f;

	void SpawnBots();

	UFUNCTION()
	void Main();

	void calculateAlignment();

	void calculateCohesion();

	void calculateSeparation();
};
