// Fill out your copyright notice in the Description page of Project Settings.


#include "BotControler.h"
#include "BotCharacter.h"
#include "Bot.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Character.h"

ABotControler::ABotControler() 
{

}

void ABotControler::BeginPlay() 
{
	Super::BeginPlay();
}

void ABotControler::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UE_LOG(LogTemp, Warning, TEXT(" Controler is ticking"));

	if (ControlledPawn->Target != nullptr)
	{
		if (ControlledPawn->behvaiour == ESteeringBehaviourType::BC_Seek)
		{
			Seek(ControlledPawn->Target);
		}
		else if (ControlledPawn->behvaiour == ESteeringBehaviourType::BC_Flee)
		{
			Flee(ControlledPawn->Target);
		}
		else if (ControlledPawn->behvaiour == ESteeringBehaviourType::BC_Arrive)
		{
			Arrive(ControlledPawn->Target, 500.0f);
		}
		else if (ControlledPawn->behvaiour == ESteeringBehaviourType::BC_Pursuit) 
		{
			Pursuit(ControlledPawn->Target);
		}
		else if (ControlledPawn->behvaiour == ESteeringBehaviourType::BC_Evade)
		{
			Evade(ControlledPawn->Target);
		}
	}

	if (ControlledPawn->behvaiour == ESteeringBehaviourType::BC_Wander) 
	{
		Wander();
	}
}

void ABotControler::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);

	ControlledPawn = Cast<ABot>(InPawn);
}

// Seek Target
void ABotControler::Seek(AActor * target)
{
	FVector ToTarget = ControlledPawn->GetActorLocation() - target->GetActorLocation();

	FVector steering = ControlledPawn->GetVelocity() - ToTarget;

	steering.Normalize();

	FVector Out = { steering.X,steering.Y, 0 };

	if (ToTarget.Size() >= ControlledPawn->AcceptanceDistance) 
	{
		ControlledPawn->AddMovementInput(Out, ControlledPawn->maxForce, true);
	}
}

void ABotControler::Seek(FVector target) 
{
	FVector ToTarget = ControlledPawn->GetActorLocation() - target;

	FVector steering = ControlledPawn->GetVelocity() - ToTarget;

	steering.Normalize();

	FVector Out = { steering.X,steering.Y, 0 };

	if (ToTarget.Size() >= ControlledPawn->AcceptanceDistance)
	{
		ControlledPawn->AddMovementInput(Out, ControlledPawn->maxForce, true);
	}
}

// Flee Target
void ABotControler::Flee(AActor * target)
{
	FVector ToTarget = ControlledPawn->GetActorLocation() - target->GetActorLocation();

	FVector steering = ControlledPawn->GetVelocity() + ToTarget;

	steering.Normalize();

	ControlledPawn->AddMovementInput(steering, ControlledPawn->maxForce, true);
}

void ABotControler::Flee(FVector target)
{
	FVector ToTarget = target;

	FVector steering = ControlledPawn->GetVelocity() + ToTarget;

	steering.Normalize();

	ControlledPawn->AddMovementInput(steering, ControlledPawn->maxForce, true);
}


void ABotControler::Arrive(AActor * target, float slowingRadius)
{
	FVector ToTarget = ControlledPawn->GetActorLocation() - target->GetActorLocation();

	if (ToTarget.Size() <= slowingRadius) 
	{
		//UE_LOG(LogTemp, Warning, TEXT(" I Am Slowing"));

		float DesiredVelocityMagnitude = ControlledPawn->maxForce*(ToTarget.Size()/ slowingRadius);

		float CurrentVelocityMagnitude = ControlledPawn->GetVelocity().Size();

		float forceToApply = DesiredVelocityMagnitude;

		FVector Direction = ControlledPawn->GetVelocity();
		Direction.Normalize();

		//UE_LOG(LogTemp, Warning, TEXT(" force being applied "));

		ControlledPawn->AddMovementInput(Direction, forceToApply, true);
	}
	else 
	{
		//UE_LOG(LogTemp, Warning, TEXT(" I Am Seeking"));

		Seek(target);
	}
}

void ABotControler::Wander()
{
	float randx = FMath::RandRange(-100, 100);
	float randy = FMath::RandRange(-100, 100);

	FVector NewTarget = { ControlledPawn->GetActorLocation().X + randx , ControlledPawn->GetActorLocation().Y + randy , ControlledPawn->GetActorLocation().Z };

	Seek(NewTarget);
}

void ABotControler::Pursuit(AActor * target)
{
	FVector FurtureTargetPosition = target->GetActorLocation() + target->GetVelocity();

	Seek(FurtureTargetPosition);
}

void ABotControler::Evade(AActor * target)
{
	FVector FurtureTargetPosition = target->GetActorLocation() + target->GetVelocity();

	Flee( -1* FurtureTargetPosition);
}

