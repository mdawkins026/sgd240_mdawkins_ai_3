// Fill out your copyright notice in the Description page of Project Settings.


#include "Bot.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "BotControler.h"
#include "ConstructorHelpers.h"

// Sets default values
ABot::ABot()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// root component
	rootComp = CreateDefaultSubobject<UCapsuleComponent>("RootComp");
	rootComp->SetCollisionProfileName(TEXT("Pawn"));
	RootComponent = rootComp;

	// movement component
	movementComp = CreateDefaultSubobject<UFloatingPawnMovement>(" MovementComp");
	movementComp->UpdatedComponent = rootComp;

	// mesh compoennt
	static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshFound(TEXT("StaticMesh'/Game/BotMesh.BotMesh'"));
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BotMesh"));
	mesh->SetStaticMesh(MeshFound.Object);
	mesh->AttachTo(RootComponent);
	mesh->SetNotifyRigidBodyCollision(true);

	// hit delegete binding
	rootComp->OnComponentHit.AddDynamic(this, &ABot::OnHitWall);

	// specifying ai controler
	this->AIControllerClass = ABotControler::StaticClass();
}

// Called when the game starts or when spawned
void ABot::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Add a gravity force
	AddMovementInput({ 0,0,-1 }, 9.8, true);

	UE_LOG(LogTemp, Warning, TEXT("Tick"));
}

// Called to bind functionality to input
void ABot::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABot::OnHitWall(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	for (int i = 0; i < walls.Num(); i++) 
	{
		if (OtherActor == walls[i]) 
		{
			AddMovementInput(-1 * walls[i]->GetActorLocation(), maxForce, true);
		}
	}
}
