// Fill out your copyright notice in the Description page of Project Settings.


#include "BotFlock.h"
#include "Components/ArrowComponent.h"
#include "Components/SceneComponent.h"
#include "DrawDebugHelpers.h"
#include "Bot.h"

// Sets default values
ABotFlock::ABotFlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComp = CreateDefaultSubobject<USceneComponent>("Root");
	RootComponent = RootComp;

	UArrowComponent* arrow = CreateDefaultSubobject<UArrowComponent>("directionalArrow");
	arrow->AttachTo(RootComponent);
}

// Called when the game starts or when spawned
void ABotFlock::BeginPlay()
{
	Super::BeginPlay();

	SpawnBots();
}

// Called every frame
void ABotFlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Main();
}

void ABotFlock::OnConstruction(const FTransform & Transform)
{
	//DrawDebugBox(GetWorld(), this->GetActorLocation(), FVector(NunberOfBotRows*IntialSpacingInterval, NunberOfBotCols*IntialSpacingInterval, 250), FColor::Purple, false, 10.0f, 0, 10);
}

void ABotFlock::SpawnBots()
{
	for (int x = 0; x < NunberOfBotRows; x++) 
	{
		for (int y = 0; y < NunberOfBotCols; y++) 
		{
			FVector Location = FVector(GetActorLocation().X + (x * IntialSpacingInterval), GetActorLocation().Y + (y * IntialSpacingInterval), GetActorLocation().Z + IntialSpacingInterval);
			FActorSpawnParameters SpawnInfo;

			ABot* currentBot = GetWorld()->SpawnActor<ABot>(Location, this->GetActorRotation(), SpawnInfo);
			currentBot->AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
			currentBot->behvaiour = behvaiour;
			currentBot->maxForce = maxForce;
			currentBot->walls = walls;
			currentBot->Target = Target;
			BotsInTheFlock.Add(currentBot);
			currentBot->SpawnDefaultController();
		}
	}

}

void ABotFlock::Main()
{
	calculateAlignment();
	calculateCohesion();
	calculateSeparation();
}

void ABotFlock::calculateAlignment()
{
	// For each bot in the flock
	for (int selectedBotIterator = 0; selectedBotIterator < BotsInTheFlock.Num(); selectedBotIterator++) 
	{
		// Varibles for search computation
		ABot* CurrentBot = BotsInTheFlock[selectedBotIterator];
		TArray<ABot*> Surrounds = TArray<ABot*>();

		// Search for all other bots in the neibourhood
		for (int i = 0; i < BotsInTheFlock.Num(); i++) 
		{
			ABot* newBot = BotsInTheFlock[i];

			// Distance between bots
			FVector DistanceVect = CurrentBot->GetActorLocation() - newBot->GetActorLocation();
			float distance = DistanceVect.Size();

			if (newBot != CurrentBot && distance <= NeighborhoodRadius) 
			{
				Surrounds.Add(newBot);
			}
		}

		// for each bot in the surrounds get average alignment and set

		FRotator AverageRoation = FRotator();

		for (int i = 0; i < Surrounds.Num(); i++)
		{
			FRotator NewRotation = Surrounds[i]->GetActorRotation();

			AverageRoation += NewRotation;
		}

		AverageRoation = (1 / Surrounds.Num())*AverageRoation;

		CurrentBot->SetActorRotation(AverageRoation);
	}
}

void ABotFlock::calculateCohesion()
{
	// For each bot in the flock
	for (int selectedBotIterator = 0; selectedBotIterator < BotsInTheFlock.Num(); selectedBotIterator++)
	{
		// Varibles for search computation
		ABot* CurrentBot = BotsInTheFlock[selectedBotIterator];
		
		// Calculate the center of mass and apply a force to keep all the bots together
		FVector AveragePosition = FVector();
		for (int i = 0; i < BotsInTheFlock.Num(); i++) 
		{
			if (BotsInTheFlock[i] != CurrentBot) 
			{
				AveragePosition += BotsInTheFlock[i]->GetActorLocation();
			}
		}
		AveragePosition = (1 / BotsInTheFlock.Num())*AveragePosition;

		// Calculate the steering vector to the new center of mass
		FVector steering = CurrentBot->GetActorLocation() - AveragePosition;
		steering.Normalize();

		// Apply the force to the bot;
		CurrentBot->AddMovementInput(-1* steering, CohesionScaler*maxForce, true);
	}
}

void ABotFlock::calculateSeparation()
{
	// For each bot in the flock
	for (int selectedBotIterator = 0; selectedBotIterator < BotsInTheFlock.Num(); selectedBotIterator++)
	{
		// Varibles for search computation
		ABot* CurrentBot = BotsInTheFlock[selectedBotIterator];
		TArray<ABot*> Surrounds = TArray<ABot*>();

		// Search for all other bots in the neibourhood
		for (int i = 0; i < BotsInTheFlock.Num(); i++)
		{
			ABot* newBot = BotsInTheFlock[i];

			// Distance between bots
			FVector DistanceVect = CurrentBot->GetActorLocation() - newBot->GetActorLocation();
			float distance = DistanceVect.Size();

			if (newBot != CurrentBot && distance <= NeighborhoodRadius)
			{
				Surrounds.Add(newBot);
			}			
		}

		// for each of the bots apply a scaled seperation force

		for (int i = 0; i < Surrounds.Num(); i++) 
		{
			// Calc the distance
			FVector distanceVect = CurrentBot->GetActorLocation() - Surrounds[i]->GetActorLocation();
			float distance = distanceVect.Size();

			// calc the force magnitude based on the distance
			float ForceScaler = SeperationScaler * (NeighborhoodRadius - distance) * (1/ NeighborhoodRadius) * maxForce;

			// Apply a force in the direction of the distance
			CurrentBot->AddMovementInput(distanceVect, ForceScaler, true);
		}
	}
}

