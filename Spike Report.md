# Spike Report

# AI3-Steering behaviours

## Introduction

This spike aims to create more realistic movement of ai characters and to improve apron the crowd mechanics by implanting Steering Behaviours.

## Goals

1. To deliver on the spike deliverables as per the spike plan.
2. To gain understanding on steering behaviors specifically the area outlined in the spike plan. This include
  1. General movement
  2. Flocking behaviors
3. To stay within the scope of this spike by following the spike plan.

## Personnel

| Primary – Matthew Dawkins | Secondary – N/A |
| --- | --- |

## Technologies, Tools, and Resources used

Tools used in this spike include:

- Adding movement input [https://docs.unrealengine.com/en-US/API/Runtime/Engine/GameFramework/APawn/AddMovementInput/index.html](https://docs.unrealengine.com/en-US/API/Runtime/Engine/GameFramework/APawn/AddMovementInput/index.html)
- Steering behavior tutorial [https://gamedevelopment.tutsplus.com/series/understanding-steering-behaviors--gamedev-12732](https://gamedevelopment.tutsplus.com/series/understanding-steering-behaviors--gamedev-12732)
- Vectors and basic vector math&#39;s [https://mathinsight.org/vector\_introduction](https://mathinsight.org/vector_introduction)
- Flocking behavior [https://gamedevelopment.tutsplus.com/tutorials/3-simple-rules-of-flocking-behaviors-alignment-cohesion-and-separation--gamedev-3444](https://gamedevelopment.tutsplus.com/tutorials/3-simple-rules-of-flocking-behaviors-alignment-cohesion-and-separation--gamedev-3444)
- Vector math&#39;s [https://docs.unrealengine.com/en-US/BlueprintAPI/Math/Vector/index.html](https://docs.unrealengine.com/en-US/BlueprintAPI/Math/Vector/index.html)







## Tasks undertaken

_The tasks undertaken will be broken up by object and then major functionality will be explained_

**Bot**

_The bot is a pawn so In order to create it we must create a new pawn ensuring that it is a child of APawn. The creation of the bot is relatively simple, with the majority of functionality being set up in the constructor in the cpp file._

![](reportImages\1.png)

_Whereby most variables are defined in the header file, next we must setup a rebounding force for whenever a bot hits the walls of the map. To do this in the header file:_

![](reportImages\2.png)

_Then in the cpp file, ensuring that the function is bound to the on component hit event_

![](reportImages\3.png)



_For further functionality we must also specify an enum which states the movement type and then some parameters for the movement itself, in the header above the class_

![](reportImages\4.png)

_And then in the class itself, these will be used by the ai controller and by the bot flock_

![](reportImages\5.png)









**BotControler**

_The controller is primarily movement methods and uses the data stored in individual bots themselves, as such in the header_

![](reportImages\6.png)

_The functionality of each movement function can be found in the steering behavior tutorial and then adapted for ue4 c++ however as for the implantation of these methods a simple state machine can used and called each tick to apply the correct method. In the cpp file directly in the tick function_

![](reportImages\7.png)

_It is also important for us to get a reference to the bot (Pawn) that is being controlled for this the following implantation in the OnPossess() function can be used._

![](reportImages\8.png)

**BotFlock**

_In order to create the flocking behavior a single actor was created that spawns a number of bots, each with their own steering behavior (using the controller) but also applying flocking forces (from the bot flock object). This keeps code concise and objects reusable. As such the Primary functionality will be explored, noting that most data is still kept in the bot pawn however it must be first set in the bot flock then written to each spawned pawn. Thus, in the header:_

![](reportImages\9.png)







_Next the bot flock must be able to spawn in the individual bots in the flock, in the header_

![](reportImages\10.png)

_Then in the cpp file, noting that many of the variables that control this_

![](reportImages\11.png)

_After this the three main forces of flocking behavior can then be applied every tick to ensure that the individual bots stay within the flock_

![](reportImages\12.png)

_Note, for implantation of each of these behaviors see the tutorial provided then adapt t0 ue4 c++. For example, the following is the separation force code adapted from the tutorial_

![](reportImages\13.png)

## What we found out

Flocking and steering behavior act as a means of implementing smooth and natural looking movement, with the ability to build on itself to create increasingly more complex movements. Further this creates more natural crowd mechanics. Overall the area in which knowledge was gained include

- Management of data between pawns and controllers
- Applying direct forces to pawns
- Get references
- Apply the steering behaviours and then flocking behaviours

##  Open Issues/risks

There is one primary issue with the code, to do with the flocking behaviors whereby some values for the parameters cause buggy behaviors or make the flock behavior seam unnatural. These include:

- Shaking or unsmooth movement
- Movement in the vertical direction
- Flocks not moving and staying stationary

The work around for this issue was setting specific values for each parameter, however this is not a long-term solution and could use with some amount of debugging.

Another issue lies in the fact that the Ai bot do not use path finding and thus they cannot handle complex environments

## Recommendations

It is recommended that the following spikes be completed to further the knowledge gained in this spike.

- Implementing steering behaviors as a movement component (Enabling pathfinding)
- Implementing better algorithms