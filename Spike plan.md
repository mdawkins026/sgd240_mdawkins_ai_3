# Spike Plan

# AI 3 – Steering Behaviours

## Context

We understand how to make our AI move and avoid each other, but now it&#39;s time to look at one of the big &quot;classic&quot; AI principles – _Steering Behaviours_.

## Grade Level

Pass

## Gap

1. Knowledge: What are Steering Behaviours
2. Skill: Implementing Steering Behaviours

## Goals/Deliverables

In a separate project:

- Build Character/AIControllers which use waypoints and basic Nav Mesh movement to build the following behaviours (blueprint or C++), and create a level which showcases:
  - Seek
  - Flee
  - Arrive
  - Wander
  - Pursuit
  - Evade
- Additionally, create a level which showcases a large number (50+) simple cone-shaped Pawns and AIControllers (in C++) using basic physics/direct movement doing the following _Flocking Behaviour_ in 3D space:
  - Alignment
  - Cohesion
  - Separation

## Dates

| Planned start date: | Week 8 |
| --- | --- |
| Deadline: | Week 9 |

## Planning Notes

1. [https://gamedevelopment.tutsplus.com/series/understanding-steering-behaviors--gamedev-12732](https://gamedevelopment.tutsplus.com/series/understanding-steering-behaviors--gamedev-12732)
2. [https://gamedevelopment.tutsplus.com/tutorials/3-simple-rules-of-flocking-behaviors-alignment-cohesion-and-separation--gamedev-3444](https://gamedevelopment.tutsplus.com/tutorials/3-simple-rules-of-flocking-behaviors-alignment-cohesion-and-separation--gamedev-3444)